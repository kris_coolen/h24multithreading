package be.kriscoolen.opdracht12;

import java.util.concurrent.*;
import java.util.*;


public class FactorialApp {

    public static void main(String[] args) throws Exception{

        FactorialCalculator fc = new FactorialCalculator(13) ;
        ExecutorService es = Executors.newSingleThreadExecutor();
        Future<Long> future = es.submit(fc);

        System.out.print("Waiting");
        while(!future.isDone()){
            Thread.sleep(1000);
            System.out.print(".");
        }

        System.out.println("\n"+future.get());



    }
}
