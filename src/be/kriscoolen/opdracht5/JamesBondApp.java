package be.kriscoolen.opdracht5;

import java.util.Random;

public class JamesBondApp {

    public static void main(String[] args) {
        Random rand = new Random();
        TimeBomb bomb = new TimeBomb(10, Thread.currentThread());
        bomb.activate();
        try {
            Thread.currentThread().sleep(rand.nextInt(15000));
            bomb.disarm();
        }catch (InterruptedException e){
            System.out.println("Bond is dead");
        }
    }
}
