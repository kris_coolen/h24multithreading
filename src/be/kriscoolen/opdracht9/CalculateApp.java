package be.kriscoolen.opdracht9;

import java.util.Random;

public class CalculateApp {
    public static void main(String[] args) {
        Calculator calc = new Calculator();
        Thread t1 = new Thread(()->calculate(calc,100));
        Thread t2 = new Thread(()->calculate(calc,100));
        Thread t3 = new Thread(()->calculate(calc,100));
        t1.setName("T1");
        t2.setName("T2");
        t3.setName("T3");
        t1.start();
        t2.start();
        t3.start();
    }

    public static void calculate(Calculator calc,int number){
        Random rand = new Random();
        for(long i = 0 ; i<number;i++){
            int value = rand.nextInt(10);
            calc.setValue(value);

            int result = calc.getResult();
            System.out.println(value + " : " + result);
            if(result!=(value*value))
                System.out.println("Error");
        }
    }
}
