package be.kriscoolen.opdracht11;

public class CounterAppLambda {
    public static void main(String[] args) throws Exception{
        Counter counter = new Counter();
        Thread t1 = new Thread(()->increment(counter,10000));
        Thread t2 = new Thread(()->increment(counter,10000));
        Thread t3  = new Thread(()->decrement(counter,20000));

        t1.start();
        t2.start();
        t3.start();
        t1.join();
        t2.join();
        t3.join();
        System.out.println(counter.getCount());



    }

    public static void increment(Counter counter, int number){
        for(int i=0; i<number; i++){
            counter.increment();
        }
    }

    public static void decrement(Counter counter, int number){
        for(int i=0; i<number; i++){
            counter.decrement();
        }
    }
}
