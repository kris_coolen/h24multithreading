package be.kriscoolen.opdracht5;


public class TimeBomb {
    int timerStart;
    int currentTime;
    Thread thread;
    Thread bondSleepThread;

    public TimeBomb(int timerStart,Thread bondSleepThread){
        this.timerStart = timerStart;
        this.currentTime = timerStart;
        this.bondSleepThread=bondSleepThread;
    }

    public void activate(){
        thread = new Thread(()->print(timerStart));
        thread.start();
    }

    public void disarm(){
        thread.interrupt();
        if(currentTime>0) System.out.println("Bond disarmed the bomb");
    }



    public void print(int count){
            for (int i = count; i >= 0; i--) {
                currentTime = i;
                System.out.println("timebomb ticking: " + i + " seconds left");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    return;

                }
            }
            System.out.println("Boooommm!!!");
            bondSleepThread.interrupt();


    }
}
