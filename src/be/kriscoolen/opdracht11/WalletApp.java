package be.kriscoolen.opdracht11;

import java.util.concurrent.*;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class WalletApp {
    public static void main(String[] args) {
        //Map<Coin,Integer> wallet = new HashMap<>();
        //Map<Coin, Integer> wallet = new LinkedHashMap<>();
        Map<Coin, Integer> wallet = new ConcurrentHashMap<>();
        wallet.put(Coin.ONE_CENT, 2);
        wallet.put(Coin.TWO_CENT, 3);
        wallet.put(Coin.FIVE_CENT, 8);
        wallet.put(Coin.TEN_CENT, 1);
        wallet.put(Coin.TWENTY_CENT, 0);
        wallet.put(Coin.FIFTY_CENT, 9);
        wallet.put(Coin.ONE_EURO, 7);
        wallet.put(Coin.TWO_EURO, 1);

        //int sum = 0;
        AtomicInteger sum = new AtomicInteger(0);
        for (Coin c : wallet.keySet()) {
            System.out.println(c.name() + " : " + wallet.get(c));
            sum.set(sum.intValue()+c.getValue()*wallet.get(c));
        }
        System.out.println((sum.intValue()/100F) + " € ");
    }
}
