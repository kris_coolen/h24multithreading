package be.kriscoolen.opdracht12;

import java.util.concurrent.Callable;

public class FactorialCalculator implements Callable<Long> {

    private long number;

    public FactorialCalculator(long number){
        this.number = number;
    }

    public Long call(){
        Long fac = new Long(1);
        for(long i=1; i<=number; i++){
            fac = fac*i;
        }
        return fac;
    }

}
