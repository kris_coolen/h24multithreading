package be.kriscoolen.opdracht7;

public class CounterAppLambda {
    public static void main(String[] args) throws Exception{
        Counter counter = new Counter();
        Thread t1 = new Thread(()->increment(counter,10000));
        Thread t2 = new Thread(()->increment(counter,10000));

        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(counter.getCount());

    }

    public static void increment(Counter counter, int number){
        for(int i=0; i<number; i++){
            counter.increment();
        }
    }
}
