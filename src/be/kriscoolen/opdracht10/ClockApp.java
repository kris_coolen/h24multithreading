package be.kriscoolen.opdracht10;

import java.util.Scanner;
import java.util.Timer;

public class ClockApp {
    public static void main(String[] args) {
        Clock clock = new Clock();
        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(clock,0,1000);
        System.out.println("Timer started");
        Scanner keyboard = new Scanner(System.in);
        keyboard.nextLine();
    }
}
