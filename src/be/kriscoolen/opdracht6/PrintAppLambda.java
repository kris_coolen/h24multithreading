package be.kriscoolen.opdracht6;

public class PrintAppLambda {
    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(()->print('#',100));
        Thread thread2 = new Thread(()->print('*',100));
        thread1.start();
        thread2.start();
        System.out.println("sleeping");
        Thread.sleep(3000);
        System.out.println("interrupting");
        thread1.interrupt();
        Thread.sleep(10000);
        if(thread1.isInterrupted()){
                System.out.println("interrupted");
        }

        try{
            thread1.join();
            thread2.join();
        } catch(InterruptedException e){}
        System.out.println("End");
    }
    public static void print(char c, int count){
        for(int i=0; i<count; i++){
            System.out.print(c);
            try{
                Thread.sleep(100);
            }catch(InterruptedException ex){}
        }
    }
}
